/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package settings;

import jaid.ais.DataSource;
import jaid.ais.DataSourceManager;
import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;
import javax.swing.DefaultListModel;

/**
 *
 * @author benjaminjakobus
 */
public class SettingsStore {

    private static SettingsStore settings;

    private SettingsStore() {
    }

    public static SettingsStore getInstance() {
        if (settings == null) {
            settings = new SettingsStore();
        }
        return settings;
    }

    public void addDataSource(DataSource src) {
        try {
            File f = new File("data/settings/" + src.getRxID() + ".ais");
            PrintWriter out = new PrintWriter(f);
            out.println(src.getIpAddress());
            out.println(src.getPort());
            out.println(src.getLocation().getLatitude());
            out.println(src.getLocation().getLongitude());
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


   public void loadSettings(DefaultListModel srcs) {
       try {
           
           // Load the data sources
            File dir = new File("data/settings/");
            Scanner scan;
            String ip, rxID;
            int port;
            double lat, lng;
            for (File f : dir.listFiles()) {
                scan = new Scanner(f);
                if (f.getName().startsWith(".") || !f.getName().endsWith(".ais")) {
                    continue;
                }
                ip = scan.nextLine();
                port = Integer.parseInt(scan.nextLine());
                lat = Double.parseDouble(scan.nextLine());
                lng = Double.parseDouble(scan.nextLine());
                rxID = f.getName().replaceFirst(".ais", "");
                DataSourceManager.getInstance().addSource(ip, port, rxID, lat, lng);
                srcs.addElement(rxID);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
   }

   public void removeSource(String rxID) {
       File f = new File("data/settings/" + rxID + ".ais");
       f.delete();
   }

   public void updateSource(DataSource src) {
       removeSource(src.getRxID());
       addDataSource(src);
   }
}
