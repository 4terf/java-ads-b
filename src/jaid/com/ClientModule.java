/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jaid.com;

/**
 * The client module interface is used to add custom message handlers or
 * parsers to clients.
 * That is, the system can receive custom data feeds, and by implementing the client module,
 * you can create custom message parsers.
 *
 * @author Benjamin Jakobus
 * @since 1.0
 * @version 1.0
 */
public interface ClientModule {

    public void interpret(String input);

}
