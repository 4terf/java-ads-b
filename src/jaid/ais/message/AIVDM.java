/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jaid.ais.message;

import jaid.ais.data.ActiveTargetsMap;

/**
 * The AIVDM class is responsible for parsing raw AIS sentences.
 * For details on how this is achieved, see ITU-R M.1371-3.
 *
 * @author Benjamin Jakobus
 * @version 1.0
 * @since 1.0
 */
public class AIVDM {

    /* The container used to track active targets. */
    private ActiveTargetsMap targetsMap;

    /* The sentence from which the AIVDM is built. */
    private String sentence;

    /* Number of parts to the message. */
    private int numMsgParts;

    /* Sentence Number. */
    private int msgPartNum;

    /* The sequential message ID for multiple sentence messages. */
    private int seqMsgID;

    /* The AIS transmission channel. */
    private String channel;

    /* The message data. */
    private String msgData;

    /* Binary message. */
    private String msgBinary;

    /* The Checksum for the most recent message. */
    private String checksum;

    /* The receiver responsible for this message */
    private String rxID;

    /* The message type (found by partial decoding). */
    private int msgType;

    /**
     * Constructor.
     * 
     * @param sentence          The binary sentence / message.
     * @param rxID              The ID of the receiver that picked up this message.
     * @since 1.0
     */
    public AIVDM(String sentence, String rxID) {
        try {
            String[] temp = sentence.split(",");
            this.sentence = sentence;
            numMsgParts = Integer.parseInt(temp[1]);
            msgPartNum = Integer.parseInt(temp[2]);
            if (temp[3].length() > 0) {
                seqMsgID = Integer.parseInt(temp[3]);
            }
            channel = temp[4];
            msgData = temp[5];
            checksum = temp[6].substring(temp[6].indexOf("*") + 1);
            this.rxID = rxID;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Parses a raw AIS feed, turning it into an AIVDM.
     * 
     * @param targetsMap            The container used to store / track
     *                              active targets.
     * @since 1.0
     */
    public void parseSentence(ActiveTargetsMap targetsMap) {
        char[] strArray;
        String binarySeg = "";
        msgBinary = "";
        AISMsg msg;
        strArray = msgData.toCharArray();
        for (int i = 0; i < strArray.length; i++) {
            int asciiNum = (int) strArray[i];
            asciiNum -= 48;
            if (asciiNum > 40) {
                asciiNum -= 8;
            }
            binarySeg = Integer.toBinaryString(asciiNum);
            if (binarySeg.length() < 6) {
                while (binarySeg.length() < 6) {
                    binarySeg = "0" + binarySeg;
                }
            }
            msgBinary += binarySeg;
        }
        // Re-interpret binary as standard 8 bit text characters...
        msgType = Integer.parseInt(msgBinary.substring(0, 6), 2);

        switch (msgType) {
            case 1:
                try {
                    msg = new AISMsg1_2_3(msgBinary);
                    targetsMap.update(msg, rxID);
                } catch (Exception e) {
                    System.out.println("Corrupt Message: (1)");
                    e.printStackTrace();
                }
                break;
            case 2:
                try {
                    msg = new AISMsg1_2_3(msgBinary);
                    targetsMap.update(msg, rxID);
                } catch (Exception e) {
                    System.out.println("Corrupt Message: (2)");
                    e.printStackTrace();
                }
                break;
            case 3:
                try {
                    msg = new AISMsg1_2_3(msgBinary);
                    targetsMap.update(msg, rxID);
                } catch (Exception e) {
                    System.out.println("Corrupt Message: (3)");
                }
                break;
            case 4:
                try {
                    msg = new AISMsg4(msgBinary);
                    targetsMap.update(msg, rxID);
                } catch (Exception e) {
                    System.out.println("Corrupt Message: (4)");
                    e.printStackTrace();
                }
                break;
            case 5:
                try {
                    msg = new AISMsg5(msgBinary);
                    targetsMap.update(msg, rxID);
                } catch (Exception e) {
                    System.out.println("Corrupt Message: (5)");
                    e.printStackTrace();
                }
                break;
            case 9:
                try {
                    msg = new AISMsg9(msgBinary);
                    targetsMap.update(msg, rxID);
                } catch (Exception e) {
                    System.out.println("Corrupt Message: (9)");
                    e.printStackTrace();
                }
                break;
            case 18:
                try {
                    msg = new AISMsg18(msgBinary);
                    targetsMap.update(msg, rxID);
                } catch (Exception e) {
                    System.out.println("Corrupt Message: (9)");
                    e.printStackTrace();
                }
                break;

            case 21:
                try {
                    msg = new AISMsg21(msgBinary);
                    targetsMap.update(msg, rxID);
                } catch (Exception e) {
                    System.out.println("Corrupt Message: (21)");
                    e.printStackTrace();
                }
                break;
        }
    }

    /**
     * Returns information (such as number of message parts, channel etc)
     * about the AIS Feed.
     *
     * @return info             Message meta-information.
     * @since 1.0
     */
    public String getInfo() {
        String outStr;
        outStr = " Input Sentence: " + sentence + "\n";
        outStr += "  Num Msg Parts: " + numMsgParts + "\n";
        outStr += "  This Msg Part: " + msgPartNum + "\n";
        outStr += "    Sequence ID: " + seqMsgID + "\n";
        outStr += "        Channel: " + channel + "\n";
        outStr += "           Data: " + msgData + "\n";
        outStr += "       Checksum: " + checksum + "\n\n";
        outStr += "   Message Type: " + msgType + "\n";
        outStr += "Binary Msg Data: " + msgBinary + "\n";
        return outStr;
    }

    /**
     * Appends a message chunk to its parent (recall that some messages may be
     * transmitted in separate chunks due to their size).
     * 
     * @param msgPart           The message chunk to append.
     * @since 1.0
     */
    public void append(AIVDM msgPart) {
        msgData += msgPart.getMsgData();
    }

    /**
     * Returns the channel information on which the message was broadcasted.
     * 
     * @return                  The channel on which this message was broadcasted.
     * @since 1.0
     */
    public String getChannel() {
        return channel;
    }

    /**
     * The message's checksum.
     *
     * @return                  The message's checksum.
     * @since 1.0
     */
    public String getChecksum() {
        return checksum;
    }

    /**
     * The AIS message in binary form.
     *
     * @return                  The AIS message in binary form.
     * @since 1.0
     */
    public String getMsgBinary() {
        return msgBinary;
    }

    public String getMsgData() {
        return msgData;
    }

    /**
     * Number of parts that compose this message (recall that message may be
     * transmitted in separate chunks due to their overall size).
     * 
     * @return                  The number of parts composing this message.
     * @since 1.0
     */
    public int getMsgPartNum() {
        return msgPartNum;
    }


    public int getNumMsgParts() {
        return numMsgParts;
    }

    /**
     * Returns the message's sequence ID (recall that sometimes messages are broken up into
     * fragements. Each fragement is assigned a seq ID to identify it for re-assembly).
     *
     * @return               The message's sequence ID.
     * @since 1.0
     */
    public int getSeqMsgID() {
        return seqMsgID;
    }
}
