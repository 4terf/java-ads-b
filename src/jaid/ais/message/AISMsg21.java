/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jaid.ais.message;

/**
 * Message 21 is a "Aids-to-navigation (AtoN)" report. This message type is
 * only transmitted by AtoN stations. These messages are transmitted autonomously
 * at a rate of once every three minutes.
 * For details see ITU-R M.1371-3.
 * 
 * @author Benjamin Jakobus
 * @version 1.0
 * @since 1.0
 */
public class AISMsg21 extends AISMsg {

    /* Vessel type. */
    private String aidType;

    /* Off position indicator. */
    private String offPosition;

    /* The AtoN type. */
    private int aidTypeCode;

    /* Denotes whether or not the AtoN is virtual. */
    private boolean virtual = false;

    /**
     * Constructs a new AIS message type 21.
     *
     * @param data      The AIS feed to parse.
     * @since 1.0
     */
    public AISMsg21(String data) {
        // Message type (bits 0-5)
        super.setMessageType(Integer.parseInt(data.substring(0, 6), 2));

        // Repeat Indicator (6-8)
        super.setRepeatIndicator(Integer.parseInt(data.substring(6, 8), 2));

        // MMSI (9-38)
        super.setMmsi(String.valueOf(Integer.parseInt(data.substring(8, 38), 2)));

        // IMO Number (38-43)
        decodeAidType(data.substring(38, 43));

        // Name (43-163)
        super.decodeName(data.substring(43, 163));

        // Position Accuracy (163-164)
        super.decodePosAccuracy(data.substring(163, 164));

        // Position
        super.decodePosition(data.substring(164, 219));

        // Vessel dimensions (219-249)
        super.decodeEntityDimensions(data.substring(219, 249));

        // EPFS type (249-253)
        super.decodeEPFSType(data.substring(249, 253));

        // Time Stamp (253-259)
        super.decodeTimeStamp(data.substring(253, 259));

        super.getPosition().associateUTCTime(super.getUtcTimeStampStr());

        // Off position indicator (259, 260)
        decodeOffPosition(data.substring(259, 260));

        // AtoN status (260-268) - NOT IN USE AT PRESENT

        // RAIM-flag (268-269)

        // Virtual AtoN flag
        int v = Integer.parseInt(data.substring(269, 270));
        if (v == 1) {
            virtual = true;
        }
    }

    @Override
    public String print() {
        String outStr = "";
        outStr += "TYPE: " + super.getMsgType() + "\n";
        outStr += " UTC: " + super.getUtcTimeStampStr() + "\n";
        outStr += "  RI: " + super.getRepeatIndicator() + "\n";
        outStr += "MMSI: " + super.getMMSI() + "\n";
        outStr += "TYPE: " + aidType + "\n";
        outStr += "NAME: " + super.getName() + "\n";
        outStr += " LAT: " + super.getPosition().toStringDegMinLat() + "\n";
        outStr += " LNG: " + super.getPosition().toStringDegMinLng() + "\n";
        outStr += "PACC: " + super.getPosAccuracy() + "\n";
        outStr += "DIMS: " + super.getA() + "m x " + super.getC() + "m\n";
        outStr += "EPFS: " + super.getEpfs() + "\n";
        outStr += "STAT: " + offPosition + "\n";
        return outStr;
    }

    /**
     * Decodes the AtoN type.
     *
     * @param input             Binary AtoN type.
     * @since 1.0
     */
    private void decodeAidType(String input) {
        aidTypeCode = Integer.parseInt(input, 2);
        switch (aidTypeCode) {
            case 0:
                aidType = "not specified";
                break;
            case 1:
                aidType = "Reference point";
                break;
            case 2:
                aidType = "RACON";
                break;
            case 3:
                aidType = "Fixed structure";
                break;
            case 5:
                aidType = "Non sectored light";
                break;
            case 6:
                aidType = "Sectored light";
                break;
            case 7:
                aidType = "Leading light front";
                break;
            case 8:
                aidType = "Leading light rear";
                break;
            case 9:
                aidType = "North cardinal beacon";
                break;
            case 10:
                aidType = "East cardinal beacon";
                break;
            case 11:
                aidType = "South cardinal beacon";
                break;
            case 12:
                aidType = "West cardinal beacon";
                break;
            case 13:
                aidType = "Port beacon";
                break;
            case 14:
                aidType = "Stbd beacon";
                break;
            case 15:
                aidType = "Preferred channel port beacon";
                break;
            case 16:
                aidType = "Preferred channel stbd beacon";
                break;
            case 17:
                aidType = "Isolated danger beacon";
                break;
            case 18:
                aidType = "Safe water beacon";
                break;
            case 19:
                aidType = "Special mark beacon";
                break;
            case 20:
                aidType = "North cardinal mark";
                break;
            case 21:
                aidType = "East cardinal mark";
                break;
            case 22:
                aidType = "South cardinal mark";
                break;
            case 23:
                aidType = "West cardinal mark";
                break;
            case 24:
                aidType = "Port mark";
                break;
            case 25:
                aidType = "Stbd mark";
                break;
            case 26:
                aidType = "Preferred channel port mark";
                break;
            case 27:
                aidType = "Preferred channel stbd mark";
                break;
            case 28:
                aidType = "Isolated danger mark";
                break;
            case 29:
                aidType = "Safe water mark";
                break;
            case 30:
                aidType = "Special mark";
                break;
            case 31:
                aidType = "Light vl/LANBY/Rig";
                break;
            default:
                aidType = "?: " + Integer.parseInt(input, 2);
        }
    }

    /**
     * Based on advise by Cormac Gebruers - decodes the off position.
     *
     * @param input         Binary.
     * @since 1.0
     */
    private void decodeOffPosition(String input) {
        switch (Integer.parseInt(input, 2)) {
            case 0:
                if (aidTypeCode > 20 && aidTypeCode < 32) {
                    offPosition = "on station";
                } else {
                    offPosition = "n/a";
                }
                break;
            case 1:
                if (aidTypeCode > 20 && aidTypeCode < 32) {
                    offPosition = "off station";
                } else {
                    offPosition = "n/a";
                }
                break;
            default:
                offPosition = Integer.parseInt(input, 2) + ": ??";
        }//switch
    }//decodeOffPosition

    @Override
    public String getName() {
        return super.getName();
    }

    /**
     * Returns the OFF Position.
     *
     * @return          The offPosition.
     * @since 1.0
     */
    public String getOffPosition() {
        return offPosition;
    }

    /**
     * Returns the AtoN type.
     *
     * @return          AtoN type.
     * @since 1.0
     */
    @Override
    public String getAidType() {
        return aidType;
    }

    /**
     *  Indicates whether this is a real or isVirtual AtoN.
     *
     * @return          <code>True</code> if this is a isVirtual AtoN; <code>false</code>
     *                  if not.
     * @since 1.0
     */
    public boolean isVirtual() {
        return virtual;
    }
}
