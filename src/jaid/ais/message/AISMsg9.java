/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jaid.ais.message;

import nav.util.string.StringUtil;

/**
 * This message is used as a standard position report for aircraft involved in SAR
 * operations. Stations other than aircraft involved in SAR operations should not
 * transmit this message. The default reporting interval for this message should be 10s.
 * For further details see ITU-R M.1371-3.
 * 
 * @author Benjamin Jakobu
 * @since 1.0
 * @version 1.0
 */
public class AISMsg9 extends AISMsg{

    /* The target's altitude. */
    private String altitude;

    /* The target's speed over ground in nautical milers per hour. */
    private int speed;

    /* The target's course over ground in degrees from 0 to 359. */
    private float course;

   /**
     * Constructs a new AIS message type 9.
     *
     * @param data      The AIS feed to parse.
     * @since 1.0
     */
    public AISMsg9(String data){
        // Message type (bits 0-5)
	super.setMessageType(Integer.parseInt(data.substring(0,6),2));

	// Repeat Indicator (7-8)
	super.setRepeatIndicator(Integer.parseInt(data.substring(6,8),2));

	// MMSI (9-38)
	StringUtil su = new StringUtil();
	super.setMmsi(su.padNumZero(Integer.parseInt(data.substring(8,38), 2),9));

	// Altitude (38-50)
	decodeAlt(data.substring(42,50));

	// Speed Over Ground (50-60)
	speed = Integer.parseInt(data.substring(50,60),2);

	// Position Accuracy (60-61)
	super.decodePosAccuracy(data.substring(60,61));

	// Position (61-116: 28 for long, 27 for lat)
	super.decodePosition(data.substring(61,116));

	// Course Over Ground (116-128)
	course = ((float)Integer.parseInt(data.substring(116,128),2))/10;

	// Time Stamp (128-134)
	super.decodeTimeStamp(data.substring(128,134));
    }

    @Override
    public String print(){
        StringUtil u = new StringUtil();
	String outStr = "";
	outStr += "TYPE: " + super.getMsgType() + "\n";
	outStr += " UTC: " + super.getUtcTimeStampStr() + "\n";
	outStr += "  RI: " + super.getRepeatIndicator() + "\n";
	outStr += "MMSI: " + super.getMMSI() + "\n";
	outStr += " SOG: " + u.padNum(speed,2,1) + "\n";
	outStr += "PACC: " + super.getPosAccuracy() + "\n";
	outStr += " LAT: " + super.getPosition().toStringDegMinLat() + "\n";
	outStr += " LNG: " + super.getPosition().toStringDegMinLng() + "\n";
	outStr += " COG: " + u.padNum(course,3,1) + "∞";
	return outStr;
    }

    /**
     * Returns the speed over ground in nautical milers per hour.
     *
     * @return speed            The speed over ground.
     * @since 1.0
     */
    @Override
    public float getSpeed(){
        return speed;
    }

    /**
     * Decodes the target's altitude.
     *
     * @param data              Binary altitude data.
     * @since 1.0
     */
    private void decodeAlt(String data){
        int val = Integer.parseInt(data,2);
	switch (val) {
            case 4095:
		altitude = "not available";
		break;
            default:
		altitude = String.valueOf(val);
	}
    }
}
