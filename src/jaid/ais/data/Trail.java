/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jaid.ais.data;

import nav.position.Position;



/**
 * Represents a vessel's individual trail.
 *
 * @author Benjamin Jakobus
 * @version 1.0
 * @since 1.0
 */
public class Trail {

    /* The trail's position. */
    private Position trailPosition;

    /* Indicates whether the trail is already drawn on the map.
     * False by default.
     */
    private boolean rendered;

    /* The trail position's (x,y) co-ordinate given the current map resolution.
     * As the resolution / zoom of the map changes, these values are re-set.
     * Although these x,y co-ordinates could be calculated within the
     * VisualTarget class for each frame drawing iteration, it is best
     * to buffer them for each position due to efficiency reasons (i.e.
     * to save them from being re-calculated for every frame).
     */
    private int x;
    private int y;


    /**
     * Constructs a new instance of <code>Trail</code>.
     * 
     * @param trailPosition         The trail's position.
     * @param rendered              <code>True</code> if drawn on the map;
     *                              <code>false if not</code>.
     * @since 1.0
     */
    public Trail(Position trailPosition, boolean rendered) {
        this.trailPosition = trailPosition;
        this.rendered = rendered;
    }

    /**
     * Constructs a new instance of <code>Trail</code>.
     *
     * @param trailPosition         The trail's position.
     * @since 1.0
     */
    public Trail(Position trailPosition) {
        this.trailPosition = trailPosition;
        this.rendered = false;
    }

    /**
     * Indicates whether or not the trail has already been drawn on the map.
     *
     * @return                      <code>True</code> if it has already been visualized;
     *                              <code>false</code> if not.
     * @since 1.0
     */
    public boolean isRendered() {
        return rendered;
    }

    /**
     * Set whether or not the trail has already been drawn on the map.
     *
     * @param rendered              <code>True</code> if it has already been visualized;
     *                              <code>false</code> if not.
     * @since 1.0
     */
    public void setRendered(boolean rendered) {
        this.rendered = rendered;
    }

    /**
     * Getter method for the trail's position.
     *
     * @return                      <code>Position</code> object denoting the
     *                              trail's position.
     * @since 1.0
     */
    public Position getTrailPosition() {
        return trailPosition;
    }

    /**
     * Set's the trail's pixel X coordinate.
     *
     * @param x                     The trail's new x coordinate.
     * @since 1.0
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Set's the trail's pixel Y coordinate.
     *
     * @param y                     The trail's new y coordinate.
     * @since 1.0
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * Returns the trail's x coordinate.
     *
     * @return                      The trail's x coordinate.
     * @since 1.0
     */
    public int getX() {
        return x;
    }

    /**
     * Returns the trail's y coordinate.
     *
     * @return                      The trail's y coordinate.
     * @since 1.0
     */
    public int getY() {
        return y;
    }
}
