 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jaid.ais.data;

import java.util.ArrayList;

/**
 * The <code>TargetsGarbageCollector</code> periodically checks the message
 * store for targets that are no longer supplying data, removing them.
 *
 * @author Benjamin Jakobus
 * @version 1.0
 * @since 1.0
 */
public class TargetsGarbageCollector implements Runnable {

    /* Timeout for target to be designated as 'lost' - in seconds! */
    private static final int TARGET_TIMEOUT = 840; //420 == 7 minutes 14m for Shanghai!

    /* The target data store to be cleaned up */
    private ActiveTargetsMap activeTargetsMap;

    /* The thread for this object */
    private Thread t;

    /**
     * Creates a new instance of <code>TargetsGarbageCollector</code>.
     *
     * @param activeTargets         A map containing all targets tracked by the
     *                              system.
     * @since 1.0
     */
    public TargetsGarbageCollector(ActiveTargetsMap activeTargets){
        activeTargetsMap = activeTargets;
        t = new Thread(this);
    }

    /**
     * Starts the garbage collector.
     * 
     * @since 1.0
     */
    public void start() {
        t.start();
    }

    /**
     * Runs the garbage collection thread. Call <code>start()</code> to run
     * the garbage collector. Do not use this method as it is used internally
     * by <code>Thread</code>.
     * 
     * @since 1.0
     */
    public void run() {
        System.out.println("Targets Garbage Collector started...");
        while (true) {
            ArrayList<Target> targets = new ArrayList<Target>(ActiveTargetsMap.getInstance().values());
            for (Target currTarget : targets) {
                if (currTarget.isLost()) {
                    ActiveTargetsMap.getInstance().remove(currTarget.getMMSI());
                } else {
                   
                    if ((currTarget.getElapsedTimeSinceLastUpdate())  > TARGET_TIMEOUT) {
                        currTarget.setLost();
                    }
                }
            }
            try {
                System.out.println("Target garbage collection cycle completed...");
                Thread.sleep(180000);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}