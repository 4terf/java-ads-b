/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jaid.ais.data;

import jaid.ais.message.*;

import java.util.concurrent.ConcurrentHashMap;

/**
 * This class uses MMSI as the primary key and maps to a TargetMessageStore
 * class that contains AIS messages for a target. The combination of
 * messages stored depends on the entity type (ship, AtoN, base station etc.),
 * and the historical data required to generate e.g. trails.
 *
 * @author Benjamin Jakobus
 * @version 1.0
 * @since 1.0
 */
public class ActiveTargetsMap extends ConcurrentHashMap<String, Target> {

    /* The instance of the targets map that is used within the application.
     * As the application employs a singleton pattern, only ever one instance
     * of this map can exist.
     */
    private static ActiveTargetsMap instance;

    /* Target field used during the update process. */
    private Target target;

    public static int messageCount;

    /**
     * Constructor. Private to allow for singleton pattern.
     */
    private ActiveTargetsMap() {
        messageCount = 0;
    }

    /**
     * Constructs a new instance of <code>ActiveTargetsMap</code>.
     *
     * @param initialCapacity       The map's initial capacity.
     * @since 1.0
     */
    private ActiveTargetsMap(int initialCapacity) {
        super(initialCapacity);
    }

    /**
     * Returns an instance of <code>ActiveTargetsMap</code>. If no instance of
     * <code>ActiveTargetsMap</code> then it will create and return a new one.
     *
     * @return              <code>ActiveTargetsMap</code> instance.
     * @since 1.0
     */
    public static ActiveTargetsMap getInstance() {
        if (instance == null) {
            instance = new ActiveTargetsMap(300);
        }
        return instance;
    }

    /**
     * Updates the targets map. This method is typically called when a
     * new AIS message is received.
     * 
     * @param latestMsg     The most recently received AIS message.
     * @param rxID          The rxID of the data source that received the
     *                      message.
     * @since 1.0
     */
    synchronized public void update(AISMsg latestMsg, String rxID) {
        //Check if it already exists in the map
        target = this.get(latestMsg.getMMSI());

        if (target == null) {
            target = new Target(latestMsg, rxID);
            this.put(latestMsg.getMMSI(), target);
            messageCount++;
        } else {
            target.update(latestMsg);
            target.updateSourceRxID(rxID);
        }
        target = null;
    }
}
