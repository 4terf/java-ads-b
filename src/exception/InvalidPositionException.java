/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package exception;

/**
 * Signals that the coordinates passed to the <code>Position</code> constructor
 * are invalid (i.e. < -90, < -180, > 90, > 180).
 *
 * @author Benjamin Jakobus
 * @since 1.0
 * @version 1.0
 */
public class InvalidPositionException extends Exception{

}



