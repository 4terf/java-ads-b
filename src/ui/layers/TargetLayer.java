/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ui.layers;

import gov.nasa.worldwind.layers.RenderableLayer;

/**
 *
 * @author Benjamin Jakobus
 * @since 2.0
 */
public class TargetLayer extends RenderableLayer {

    private static TargetLayer targetLayer;

    private TargetLayer() {
    }

    public static TargetLayer getInstance() {
        if (targetLayer == null) {
            targetLayer = new TargetLayer();
        }
        return targetLayer;
    }

}
