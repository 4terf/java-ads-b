/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ui.layers;

import gov.nasa.worldwind.layers.AnnotationLayer;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.GeographicText;
import gov.nasa.worldwind.render.GeographicTextRenderer;
import gov.nasa.worldwind.util.Logging;
import java.util.Collection;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 *
 * @author benjaminjakobus
 */
public class AircraftTextLayer extends AnnotationLayer {

    private static AircraftTextLayer textLayer;

    private AircraftTextLayer() {
    }

    public static AircraftTextLayer getInstance() {
        if (textLayer == null) {
            textLayer = new AircraftTextLayer();
        }
        return textLayer;
    }


}
