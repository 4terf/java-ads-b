/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ui.layers;

import gov.nasa.worldwind.layers.AnnotationLayer;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.GeographicText;
import gov.nasa.worldwind.render.GeographicTextRenderer;
import gov.nasa.worldwind.util.Logging;
import java.util.Collection;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 *
 * @author benjaminjakobus
 */
public class TargetTextLayer extends AnnotationLayer {

    private static TargetTextLayer textLayer;

    private TargetTextLayer() {
    }

    public static TargetTextLayer getInstance() {
        if (textLayer == null) {
            textLayer = new TargetTextLayer();
        }
        return textLayer;
    }


}
