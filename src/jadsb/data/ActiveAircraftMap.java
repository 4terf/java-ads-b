/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jadsb.data;

import jadsb.adsb.message.ADSBMessage;
import java.util.Hashtable;

/**
 * Tracks active aircrafts.
 *
 * @author Benjamin Jakobus
 * @since 2.0
 */
public class ActiveAircraftMap extends Hashtable<String, Aircraft>{
    private static ActiveAircraftMap map;

    private ActiveAircraftMap() {
    }

    public static ActiveAircraftMap getInstance() {
        if (map == null) {
            map = new ActiveAircraftMap();
        }
        return map;
    }

    public void assignToAircraft(ADSBMessage msg) {
        Aircraft aircraft = get(msg.getAircraftID());
        if (aircraft == null) {
            aircraft = new Aircraft(msg.getAircraftID(), msg.getFlightID());
            put(msg.getAircraftID(), aircraft);
        }
        aircraft.update(msg);
    }



}
