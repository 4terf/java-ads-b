/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jadsb.adsb.message;

import nav.position.Position;
import nav.util.math.NavCalculator;

/**
 *
 * @author Benjamin Jakobus
 * @since 2.0
 */
public class ADSBMsg3 extends ADSBMessage {

    private Position pos;

    public ADSBMsg3(String[] contents) {
        super(contents);

        assignPosition();
    }

    private void assignPosition() {
        try {
            pos = new Position(Double.parseDouble(contents[14]), Double.parseDouble(contents[15]));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Position getPosition() {
        return pos;
    }

    /**
     * Returns the mode C altitude. Height relative to 1013.2mb (Flight Level).
     *
     * @return
     * @since 2.0
     */
    public String getAltitude() {
        return contents[11];
    }

    public double getAltitudeInMeters() {
        double mb = Double.parseDouble(getAltitude());
        double inhg = NavCalculator.mbarToHG(mb);
        return NavCalculator.hgToMeters(inhg);
    }

    public boolean hasSentEmergencyCode() {
        if (contents[19].equals("1")) {
            return true;
        }
        return false;
    }

    public boolean isOnGround() {
        if (contents[21].equals("1")) {
            return true;
        }
        return false;
    }

    @Override
    public int getType() {
        return 3;
    }
}
