package jadsb.adsb.message;

import nav.position.Position;
import nav.util.math.NavCalculator;

/**
 *
 * @author Benjamin Jakobus
 * @since 2.0
 */
public class ADSBMsg2 extends ADSBMessage {

    private Position pos;

    public ADSBMsg2(String[] contents) {
        super(contents);
    }

    private void assignPosition() {
        try {
            pos = new Position(Double.parseDouble(contents[14]), Double.parseDouble(contents[15]));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the mode C altitude. Height relative to 1013.2mb (Flight Level).
     *
     * @return          The mode C altitude. Height relative to 1013.2mb (Flight Level).
     * @since 2.0
     */
    public String getAltitude() {
        return contents[11];
    }

    public double getAltitudeInMeters() {
        double mb = Double.parseDouble(getAltitude());
        double inhg = NavCalculator.mbarToHG(mb);
        return NavCalculator.hgToMeters(inhg);
    }

    /**
     * Returns the speed over ground (not indicated airspeed).
     * 
     * @return          Speed over ground (not indicated airspeed).
     * @since 2.0
     */
    public String getSpeed() {
        return contents[12];
    }

    /**
     * The track of the aircraft.
     *
     * @return          The track.
     * @since 2.0
     */
    public double getTrack() {
        return Double.parseDouble(contents[13]);
    }

    public Position getPosition() {
        return pos;
    }

    public boolean isOnGround() {
        if (contents[21].equals("1")) {
            return true;
        }
        return false;
    }

    @Override
    public int getType() {
        return 2;
    }
}
