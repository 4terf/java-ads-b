/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jadsb.adsb.message;

import nav.util.math.NavCalculator;

/**
 *
 * @author Benjamin Jakobus
 * @since 2.0
 */
public class ADSBMsg5 extends ADSBMessage {

    public ADSBMsg5(String[] contents) {
        super(contents);
    }

    /**
     * Returns the mode C altitude. Height relative to 1013.2mb (Flight Level).
     *
     * @return
     * @since 2.0
     */
    public String getAltitude() {
        return contents[11];
    }

    public double getAltitudeInMeters() {
        double mb = Double.parseDouble(getAltitude());
        double inhg = NavCalculator.mbarToHG(mb);
        return NavCalculator.hgToMeters(inhg);
    }

    public boolean isOnGround() {
        if (contents[21].equals("1")) {
            return true;
        }
        return false;
    }

    @Override
    public int getType() {
        return 5;
    }
}
