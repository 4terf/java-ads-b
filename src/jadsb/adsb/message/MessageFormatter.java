/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jadsb.adsb.message;

/**
 *
 * @author Benjamin Jakobus
 * @since 2.0
 */
public class MessageFormatter {

    public ADSBMessage extractMessage(String input) {
        ADSBMessage msg;
        String[] contents = input.split(",");
        if (contents.length < 1 || !contents[0].equals("MSG")) {
            return null;
        }
        int msgID = Integer.parseInt(contents[1]);
        
        switch (msgID) {
            case 1:
                msg = new ADSBMsg1(contents);
                break;
            case 3:
                msg = new ADSBMsg3(contents);
                break;
            case 4:
                msg = new ADSBMsg4(contents);
                break;
            default:
                msg = null;
        }
        return msg;
        
    }

}
