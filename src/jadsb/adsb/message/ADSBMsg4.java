/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jadsb.adsb.message;

/**
 *
 * @author Benjamin Jakobus
 * @since 2.0
 */
public class ADSBMsg4 extends ADSBMessage {

    public ADSBMsg4(String[] contents) {
        super(contents);
    }

    /**
     * Returns the speed over ground (not indicated airspeed).
     *
     * @return          Speed over ground (not indicated airspeed).
     * @since 2.0
     */
    public String getSpeed() {
        return contents[12];
    }

    @Override
    public int getType() {
        return 4;
    }

    public double getTrack() {
        return Double.parseDouble(contents[13]);
    }
}
